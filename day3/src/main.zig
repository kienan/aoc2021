const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    // Part 1
    // gamma_rate * epsilon_rate = power_consumption
    // gamma_rate = for each bit position in a number
    //   consider all numbers, this bit position is the most common
    // epsilon_rate = for each bit position in a number
    //   consider all numbers, this bit position is the least common

    var it = std.mem.tokenize(contents, "\n");
    var first = it.next().?;
    var length = first.len;
    var n_zero_bits = try std.ArrayList(u32).initCapacity(alloc, length);
    defer n_zero_bits.deinit();
    n_zero_bits.appendNTimesAssumeCapacity(0, length);
    var n_total_numbers: u32 = 0;

    // This is used in the 2nd part
    var numbers = std.ArrayList(u32).init(alloc);

    it = std.mem.tokenize(contents, "\n");
    while (it.next()) |line| {
        n_total_numbers += 1;
        try numbers.append(try std.fmt.parseInt(u32, line, 2));
        for (line) |c, i| {
            if (c == '0') {
                n_zero_bits.items[i] += 1;
            }
        }
    }
    //std.log.debug("{} items processed; n_zero: '{any}'",
    //              .{n_total_numbers, n_zero_bits.items});
    var gamma: u32 = 0;
    var epsilon: u32 = 0;
    var half = n_total_numbers / 2;
    for (n_zero_bits.items) |value, i| {
        // Not clear what should happen if the bits are
        // equally common, but we do not appear to have
        // that case in our input.
        if(value > half) {
            gamma = gamma |  (@as(u32, 0) << @intCast(u5, length - i - 1));
            epsilon = epsilon | (@as(u32, 1) << @intCast(u5, length - i - 1));
        }
        else {
            gamma = gamma | (@as(u32, 1) << @intCast(u5, length - i - 1));
            epsilon = epsilon | (@as(u32, 0) << @intCast(u5, length - i - 1));
        }
        //std.log.debug("After round {}, gamma: {b}, epsilon: {b}",
        //              .{i, gamma, epsilon});
    }
    std.log.info("[Part 1] Gamma: {}, Epsilon: {}, Product: {}",
                 .{gamma, epsilon, gamma * epsilon});

    // Part 2
    var scrubber_rating: u32 = 0;
    var generator_rating: u32 = 0;
    var index: i32 = @intCast(i32, length) - 1;
    var generator_values = try std.mem.dupe(alloc, u32, numbers.items);
    defer alloc.free(generator_values);
    var scrubber_values = try std.mem.dupe(alloc, u32, numbers.items);
    defer alloc.free(scrubber_values);
    //std.log.warn("G: {b:0>5}", .{generator_values});
    //std.log.warn("S: {b:0>5}", .{scrubber_values});
    while (index >= 0) : (index -= 1) {
        if (generator_values.len > 1) {
            var generator_result = try filter(
                alloc, generator_values, @intCast(u32, index), true);
            //std.log.warn("G: {b:0>5}", .{generator_result});
            alloc.free(generator_values);
            generator_values = generator_result;
        }
        if (scrubber_values.len > 1) {
            var scrubber_result = try filter(
                alloc, scrubber_values, @intCast(u32, index), false);
            //std.log.warn("S: {b:0>5}", .{scrubber_result});
            alloc.free(scrubber_values);
            scrubber_values = scrubber_result;
        }
    }
    //std.log.debug("Generator values: {any}\nScrubber values: {any}",
    //              .{generator_values, scrubber_values});
    std.log.info("[Part 2] Generator: {}, Scrubber {}, Product {}",
                 .{generator_values[0], scrubber_values[0],
                   generator_values[0] * scrubber_values[0],});
}

/// caller owns returned memory
pub fn filter(alloc: *std.mem.Allocator, values: []u32, index: u32, most_common: bool) ![]u32 {
    var n_one: u32 = 0;
    var n_zero: u32 = 0;
    // One pass to count
    for (values) |v| {
        var r =  v & (@as(u32, 1) << @intCast(u5, index));
        //std.log.warn("{b} & {b} = {b}", .{
        //    v, @as(u32, 1) << @intCast(u5, index), r});
        if (v & (@as(u32, 1) << @intCast(u5, index)) != 0) {
            n_one += 1;
        }
    }
    n_zero = @intCast(u32, values.len) - n_one;
    //std.log.warn("{}, {}", .{n_zero, n_one});
    // One pass to filter into new slice
    var new_values = std.ArrayList(u32).init(alloc);
    defer new_values.deinit();
    var most_common_value: u32 = 0;
    if (n_zero < n_one) {
        if (most_common) {
            most_common_value = 1;
        }
        else {
            most_common_value = 0;
        }
    }
    else if (n_zero == n_one) {
        if (most_common) {
            most_common_value = 1;
        }
        else {
            most_common_value = 0;
        }
    }
    else {
        if (most_common) {
            most_common_value = 0;
        }
        else {
            most_common_value = 1;
        }
    }
    // std.log.warn("Most common value at index {} is {}",
    //              .{index, most_common_value});
    for (values) |v, i| {
        var bit_at_index = (v & (@as(u32, 1) << @intCast(u4, index)))
            >> @intCast(u4, index);
        //std.log.warn("Value {} Bit at index {} is {b}", .{i, index, bit_at_index});
        if (bit_at_index == most_common_value) {
            try new_values.append(v);
        }
    }
    return new_values.toOwnedSlice();
}

test "filter most common" {
    var values = [_]u32 {
        0b00100,
        0b11110,
        0b10110,
        0b10111,
        0b10101,
        0b01111,
        0b00111,
        0b11100,
        0b10000,
        0b11001,
        0b00010,
        0b01010,
    };
    var most_common_at_4 = [_]u32 {
        0b11110,
        0b10110,
        0b10111,
        0b10101,
        0b11100,
        0b10000,
        0b11001,
    };
    var alloc = std.testing.allocator;
    var result = try filter(alloc, values[0..], 4, true);
    try std.testing.expectEqual(true, std.mem.eql(u32, most_common_at_4[0..], result[0..]));
    alloc.free(result);

    // Example with a tie
    var tie_values = [_] u32 {
        0b10110,
        0b10111,
    };
    var most_common_at_0 = [_]u32 {
        0b10111,
    };
    result = try filter(alloc, tie_values[0..], 0, true);
    defer alloc.free(result);
    try std.testing.expectEqual(true, std.mem.eql(u32, most_common_at_0[0..], result[0..]));
}

test "filter least common" {
    var values = [_]u32 {
        0b00100,
        0b11110,
        0b10110,
        0b10111,
        0b10101,
        0b01111,
        0b00111,
        0b11100,
        0b10000,
        0b11001,
        0b00010,
        0b01010,
    };
    var least_common_at_4 = [_]u32 {
        0b00100,
        0b01111,
        0b00111,
        0b00010,
        0b01010,
    };
    var alloc = std.testing.allocator;
    var result = try filter(alloc, values[0..], 4, false);
    //std.log.warn("{b}", .{result});
    try std.testing.expectEqual(true, std.mem.eql(u32, least_common_at_4[0..], result[0..]));
    alloc.free(result);

    // Example with a tie
    var tie_values = [_] u32 {
        0b01111,
        0b01010,
    };
    var least_common_at_2 = [_]u32 {
        0b01010,
    };
    result = try filter(alloc, tie_values[0..], 2, false);
    //std.log.warn("{any}", .{result});
    defer alloc.free(result);
    try std.testing.expectEqual(true, std.mem.eql(u32, least_common_at_2[0..], result[0..]));
}

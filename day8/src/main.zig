const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    var digits_in_output: u16 = 0;
    var it = std.mem.tokenize(contents, "\n");
    while (it.next()) |line| {
        var lit = std.mem.tokenize(line, "|");
        _ = lit.next(); // skip output
        var oit = std.mem.tokenize(lit.next().?, " ");
        while (oit.next()) |digit| {
            // 1: 2 segments
            // 7: 3 segments
            // 4: 4 segments
            // 8: 7 segments
            //std.log.debug("'{s}': {} digits", .{digit, digit.len});
            if (digit.len <= 4 or digit.len == 7) {
                digits_in_output += 1;
            }
        }
    }
    std.log.info("[Part 1] Output values contain {} digits that are 1, 4, 7 or 8",
                 .{digits_in_output});

    it = std.mem.tokenize(contents, "\n");
    var output: u32 = 0;
    while (it.next()) |line| {
        // Each line in 'input | output', and has a different mapping
        var lit = std.mem.tokenize(line, "|");
        var o: u32 = try find_output_value(alloc, lit.next().?, lit.next().?);
        output += 0;
        break;
    }
}

fn find_output_value(alloc: *std.mem.Allocator, inputs: []const u8, outputs: []const u8) !u32 {
    // we use ips for storing both inputs and outputs to try and
    // solve which letters correspond to which display segment index
    var ips: [14][]const u8 = undefined;
    var ops: [4][]const u8 = undefined;

    // get pointers to our ips and ops
    var it = std.mem.tokenize(inputs, " ");
    var i: usize = 0;
    while (it.next()) |ip| {
        ips[i] = ip;
        i += 1;
    }
    it = std.mem.tokenize(outputs, " ");
    i = 0;
    while (it.next()) |op| {
        ops[i] = op;
        ips[i+10] = op;
        i += 1;
    }

    for (ips) |v, k| {
        std.log.debug("Input {}: {s}", .{k, v});
    }
    for (ops) |v, k| {
        std.log.debug("Output {}: {s}", .{k, v});
    }
    // segments indices:
    //   0
    // 1   2
    //   3
    // 4   5
    //   6
    // for each index, we can have some number of options
    var segment_possibilities: [7]std.AutoHashMap(u8, void) = undefined;
    i = 0;
    while (i < 7) : (i += 1) {
        segment_possibilities[i] = std.AutoHashMap(u8, void).init(alloc);
        try segment_possibilities[i].ensureCapacity(7);
    }
    for (ips) |v| {
        if (v.len == 2) {
            segment_possibilities[2].putAssumeCapacity(v[0], undefined);
            segment_possibilities[2].putAssumeCapacity(v[1], undefined);
            segment_possibilities[5].putAssumeCapacity(v[0], undefined);
            segment_possibilities[5].putAssumeCapacity(v[1], undefined);
        }
        if (v.len == 3) {
            segment_possibilities[0].putAssumeCapacity(v[0], undefined);
            segment_possibilities[0].putAssumeCapacity(v[1], undefined);
            segment_possibilities[0].putAssumeCapacity(v[2], undefined);
            segment_possibilities[2].putAssumeCapacity(v[0], undefined);
            segment_possibilities[2].putAssumeCapacity(v[1], undefined);
            segment_possibilities[2].putAssumeCapacity(v[2], undefined);
            segment_possibilities[5].putAssumeCapacity(v[0], undefined);
            segment_possibilities[5].putAssumeCapacity(v[1], undefined);
            segment_possibilities[5].putAssumeCapacity(v[2], undefined);
        }
        if (v.len == 4) {
            segment_possibilities[1].putAssumeCapacity(v[0], undefined);
            segment_possibilities[1].putAssumeCapacity(v[1], undefined);
            segment_possibilities[1].putAssumeCapacity(v[2], undefined);
            segment_possibilities[1].putAssumeCapacity(v[3], undefined);
            segment_possibilities[2].putAssumeCapacity(v[0], undefined);
            segment_possibilities[2].putAssumeCapacity(v[1], undefined);
            segment_possibilities[2].putAssumeCapacity(v[2], undefined);
            segment_possibilities[2].putAssumeCapacity(v[3], undefined);
            segment_possibilities[4].putAssumeCapacity(v[0], undefined);
            segment_possibilities[4].putAssumeCapacity(v[1], undefined);
            segment_possibilities[4].putAssumeCapacity(v[2], undefined);
            segment_possibilities[4].putAssumeCapacity(v[3], undefined);
            segment_possibilities[5].putAssumeCapacity(v[0], undefined);
            segment_possibilities[5].putAssumeCapacity(v[1], undefined);
            segment_possibilities[5].putAssumeCapacity(v[2], undefined);
            segment_possibilities[5].putAssumeCapacity(v[3], undefined);

        }
    }
    i = 0;
    while (i < 7) : (i += 1) {
        segment_possibilities[i].deinit();
    }
    return 0;
}

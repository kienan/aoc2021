const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);
    var it = std.mem.tokenize(contents, ",");

    // not unsurprisingly, tracking all the fish individually
    // runs out of memory. maybe not? I might have misunderstood
    // an error regarding parsing of a integer with a string
    // that has a newline in it.
    // good thing I abandoned this approach, since part 2
    // clearly is larger than the memory I have available.
    // var fish = std.ArrayList(i8).init(alloc);
    // defer fish.deinit();

    // while (it.next()) |fi| {
    //     try fish.append(try std.fmt.parseInt(i8, fi, 10));
    // }

    // var n_cycles: u8 = 0;
    // while (n_cycles < 80) : (n_cycles += 1) {
    //     var i: usize = 0;
    //     var len = fish.items.len;
    //     while (i < len) {
    //         fish.items[i] -= 1;
    //         if (fish.items[i] < 0) {
    //             fish.items[i] = 6;
    //             try fish.append(8);
    //         }
    //     }
    // }

    // std.log.info("[Part 1] After {} cycles, there are {} fish",
    //              .{n_cycles, fish.items.len});

    var days = [_] u64 {
        0, // 0: ready to spawn
        0, // 1: day 0
        0, // ...
        0,
        0,
        0,
        0,
        0, // 7: day 6
        0,
        0, // 9: day 8
    };
    while (it.next()) |fi| {
        var val = try std.fmt.parseInt(u64, fi[0..1], 10);
        days[val+1] += 1; // our index is val+1, since
        // we have slot 0 storing currently reproducing
        // fish
    }

    var n_cycles: u16 = 0;
    while (n_cycles < 80) : (n_cycles += 1) {
        var i: usize = 1;
        while (i < days.len) : (i += 1) {
            // Move all fish from i to i-1
            days[i-1] = days[i];
        }
        // Add new fish to days[9]
        days[9] = days[0];
        // Move fish from days[0] to days[7]
        days[7] += days[0];
        days[0] = 0;
    }
    var sum: u64 = 0;
    for (days) |v| {
        sum += v;
    }
    std.log.info("[Part 1] After {} cycles, there are {} fish",
                 .{n_cycles, sum});
    while (n_cycles < 256) : (n_cycles += 1) {
        var i: usize = 1;
        while (i < days.len) : (i += 1) {
            // Move all fish from i to i-1
            days[i-1] = days[i];
        }
        // Add new fish to days[9]
        days[9] = days[0];
        // Move fish from days[0] to days[7]
        days[7] += days[0];
        days[0] = 0;
    }
    sum = 0;
    for (days) |v| {
        sum += v;
    }
    std.log.info("[Part 2] After {} cycles, there are {} fish",
                 .{n_cycles, sum});
}

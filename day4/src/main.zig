const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    var numbers = std.ArrayList(u8).init(alloc);
    defer numbers.deinit();
    var boards = std.ArrayList(Board).init(alloc);
    boards.deinit();

    var it = std.mem.tokenize(contents, "\n");
    var number_line = it.next().?;
    var number_it = std.mem.tokenize(number_line, ",");
    while (number_it.next()) |line| {
        try numbers.append(try std.fmt.parseInt(u8, line, 10));
    }

    var current_board: Board = try Board.init(alloc);
    var current_number: u8 = 0;
    while (it.next()) |line| {
        var line_it = std.mem.tokenize(line, " ");
        var row = current_number / 5;
        while (line_it.next()) |n| {
            var col = current_number % 5;
            current_number += 1;
            current_board.add_number(row, col, try std.fmt.parseInt(u8, n, 10));
        }
        if (current_number == 25) {
            current_number = 0;
            try boards.append(current_board);
            current_board = try Board.init(alloc);
        }
    }
    std.log.debug("{} numbers, {} boards in play", .{numbers.items.len,
                                                     boards.items.len});
    const BoardWinState = struct {
        index: usize,
        call: u8,
        state: [5][5]bool,
    };
    var boards_that_have_won = std.AutoHashMap(usize, void).init(alloc);
    defer boards_that_have_won.deinit();
    try boards_that_have_won.ensureCapacity(@intCast(u32, boards.items.len));
    var winners = std.ArrayList(BoardWinState).init(alloc);
    defer winners.deinit();
    try winners.ensureCapacity(@intCast(u32, boards.items.len));
    for (numbers.items) |value, index| {
        // std.log.debug("Running iteration {}, value {}", .{index, value});
        for (boards.items) |board, i| {
            if (board.board_numbers.get(value)) |point| {
                //std.log.debug("Board {} has {} at point {any}",
                //              .{i, value, point});
                boards.items[i].mark_seen(point);
            }
        }
        if (index >= 5) {
            for (boards.items) |board, i| {
                // Check if we know this winner
                if (boards_that_have_won.get(i)) |b_index| {
                    continue;
                }
                if (boards.items[i].has_won()) {
                    //std.log.debug("Board {} has just won on iter {} call {}",
                    //              .{i, index, value});
                    boards_that_have_won.putAssumeCapacityNoClobber(i, undefined);
                    winners.appendAssumeCapacity(.{
                        .index = i,
                        .call = value,
                        .state = boards.items[i].row_col_marked,
                    });
                }
            }
        }
    }
    // Winner
    var winner_value = boards.items[winners.items[0].index].value_at_state(
        winners.items[0].state);
    std.log.info("[Part 1] Board {} won with call {}, unmarked value {}, product {}",
                 .{winners.items[0].index, winners.items[0].call, winner_value,
                   winner_value * winners.items[0].call});

    // Last winner
    var last_winner = winners.items.len - 1;
    winner_value = boards.items[winners.items[last_winner].index].value_at_state(
        winners.items[last_winner].state);
    std.log.info("{}: {any}", .{last_winner, winners.items[last_winner]});
    std.log.info(
        "[Part 2] Board {} won last with call {}, unmarked value {}, product {}",
        .{winners.items[last_winner].index, winners.items[last_winner].call,
          winner_value, winner_value * winners.items[last_winner].call}
    );

    // Cleanup
    for (boards.items) |b, i| {
        boards.items[i].board_numbers.deinit();
    }
}

pub const Point = struct {
    row: u8 = 0,
    col: u8 = 0,
};
const FullRow = [_] bool {
    true, true, true, true, true
};
pub const Board = struct {
    row_col_marked: [5][5] bool,
    board_numbers: std.AutoHashMap(u8, Point),
    const Self = @This();
    pub fn init(alloc: *std.mem.Allocator) !Self {
        var self = Board {
            .row_col_marked = std.mem.zeroes([5][5] bool),
            .board_numbers = std.AutoHashMap(u8, Point).init(alloc),
        };
        try self.board_numbers.ensureCapacity(25);
        return self;
    }

    pub fn deinit(alloc: *std.mem.Allocator) void {
        self.board_numbers.deinit(alloc);
    }

    pub fn add_number(self: *Self, row: u8, col: u8, value: u8) void {
        var p = Point { .row = row, .col = col, };
        self.board_numbers.putAssumeCapacityNoClobber(value, p);
    }

    pub fn mark_seen(self: *Self, p: Point) void {
        self.row_col_marked[p.row][p.col] = true;
    }

    pub fn get_value(self: *Self) u32 {
        var value: u32 = 0;
        var it = self.board_numbers.iterator();
        while (it.next()) |kv| {
            if(!self.row_col_marked[kv.value_ptr.*.row][kv.value_ptr.*.col]) {
                value += kv.key_ptr.*;
            }
        }
        return value;
    }

    pub fn value_at_state(self: *Self, state: [5][5]bool) u32 {
        var value: u32 = 0;
        var it = self.board_numbers.iterator();
        while (it.next()) |kv| {
            if(!state[kv.value_ptr.*.row][kv.value_ptr.*.col]) {
                value += kv.key_ptr.*;
            }
        }
        return value;
    }

    // pub fn print(self: *Self) void {
    //     const stdout = std.io.getStdOut();
    //     const held = std.debug.getStdoutMutex().acquire();
    //     defer held.release();
    //     var r: u8 = 0;
    //     while (r < 5) : (r += 1) {
    //         stdout.print("{any}", .{self.row_col_marked[r]});
    //     }
    // }

    pub fn has_won(self: *Self) bool {
        var i: usize = 0;
        var data: [5]bool = undefined;
        while (i < 5) : (i += 1) {
            data = self.row_col_marked[i];
            if (std.mem.eql(bool, data[0..], FullRow[0..])) {
                //std.log.debug("Row {} is filled", .{i});
                return true;
            }
            data = [_] bool {
                self.row_col_marked[0][i],
                self.row_col_marked[1][i],
                self.row_col_marked[2][i],
                self.row_col_marked[3][i],
                self.row_col_marked[4][i],
            };
            //std.log.debug("Row / Col {}: {any} / {any}",
            //              .{i, self.row_col_marked[i], data});
            if (std.mem.eql(bool, data[0..], FullRow[0..])) {
                //std.log.debug("Col {} is filled", .{i});
                return true;
            }
        }
        return false;
    }
};

const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    var points = std.AutoHashMap(Point, u8).init(alloc);
    defer points.deinit();
    var points_p2 = std.AutoHashMap(Point, u8).init(alloc);
    defer points_p2.deinit();

    var it = std.mem.tokenize(contents, "\n");
    while (it.next()) |line| {
        var begin: Point = undefined;
        var end: Point = undefined;
        var lit = std.mem.tokenize(line, " ");
        var iter: u8 = 0;
        var diagonal: bool = false;
        while (lit.next()) |segment| {
            if (iter % 2 == 0) {
                var sit = std.mem.tokenize(segment, ",");
                var x: i16 = try std.fmt.parseInt(i16, sit.next().?, 10);
                var y: i16 = try std.fmt.parseInt(i16, sit.next().?, 10);
                if (iter == 0) {
                    begin = .{ .x = x, .y = y };
                }
                else {
                    end = .{ .x = x, .y = y };
                }
            }
            iter += 1;
        }
        var step = Point { .x = 0, .y = 0};
        var steps_required: i16 = 0;
        if (begin.x != end.x and begin.y != end.y) {
            // Diagonal line
            diagonal = true;
            step = Point { .x = end.x - begin.x, .y = end.y - begin.y };
            //std.log.warn("Diagonal line from {any} to {any} detected, step start {}",
            //             .{begin, end, step});
            var sx = try std.math.absInt(step.x);
            var sy = try std.math.absInt(step.y);
            std.debug.assert(sx == sy);
            // This means our step is always +/-1,+/-1. Based on the input,
            // there aren't diagonals that are +/-N,+/-N2 where N, N2 != 1
            steps_required = sx;
            step = .{ .x = @divFloor(step.x, sx), .y = @divFloor(step.y, sx) };
        }
        else {
            step = Point { .x = end.x - begin.x, .y = end.y - begin.y };
            steps_required = try std.math.absInt(step.x + step.y );
            if (step.x != 0) {
                step.x = @divFloor(step.x, try std.math.absInt(step.x));
            }
            if (step.y != 0) {
                step.y = @divFloor(step.y, try std.math.absInt(step.y));
            }
            //std.log.debug("Line {any} to {any} step is {any}",
            //              .{begin, end, step});
        }
        var p: Point = begin;
        var step_iter: u16 = 0;
        while (step_iter <= steps_required) : (step_iter += 1) {
            //std.log.debug("Adding point {any}", .{p});
            try register_point(&points_p2, p);
            if (!diagonal) {
                try register_point(&points, p);
            }
            p = p.add(step);
        }
    }

    // Iterate over our points
    var n_dangerous_points: u32 = 0;
    var pit = points.iterator();
    while (pit.next()) |kv| {
        if (kv.value_ptr.* >= 2) {
            n_dangerous_points += 1;
        }
    }
    std.log.info("[Part 1] There are {} dangerous points", .{n_dangerous_points});

    n_dangerous_points = 0;
    pit = points_p2.iterator();
    while (pit.next()) |kv| {
        if (kv.value_ptr.* >= 2) {
            n_dangerous_points += 1;
        }
    }
    std.log.info("[Part 2] There are {} dangerous points", .{n_dangerous_points});
}

pub fn register_point(points: *std.AutoHashMap(Point, u8),p: Point) !void {
    if (points.*.get(p)) |n| {
        try points.*.put(p, n+1);
    }
    else {
        try points.*.put(p, 1);
    }
}

pub const Point = struct {
    x: i16,
    y: i16,

    pub fn eql(p1: Point, p2: Point) bool {
        return p1.x == p2.x and p1.y == p2.y;
    }

    pub fn add(p1: Point, p2: Point) Point {
        return .{
            .x = p1.x + p2.x,
            .y = p1.y + p2.y,
        };
    }
};

pub const Line = struct {
    a: Point,
    b: Point,
};
